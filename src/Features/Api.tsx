import React, { Component } from "react";

class App extends Component {
  state = {
    cards: []
  };

  // Code is invoked after the component is mounted/inserted into the DOM tree.
  componentDidMount() {
    const url = "https://api.magicthegathering.io/v1/cards?name=thalia";

    fetch(url)
      .then(result => result.json())
      .then((result: { cards: [] }) => {
        this.setState({
          cards: result.cards
        });
      });
  }

  render() {
    const { cards } = this.state;
    const result = cards.map((entry: any, index) => {
      return <li key={index}>{entry.name}</li>;
    });

    return <ul>{result}</ul>;
  }
}

export default App;
