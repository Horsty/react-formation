import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
// import App from "./Features/App";
import Api from "./Features/Api";

ReactDOM.render(<Api />, document.getElementById("root"));
// ReactDOM.render(<App />, document.getElementById("root"));
